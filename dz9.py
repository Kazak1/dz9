def fibonachi(iteration, n2=1, n1=1):
    s = n1 + n2
    n1 = n2
    n2 = s
    iteration -= 1
    if iteration > 0:
        n2 = fibonachi(iteration, n2, n1)
    return n2


def min_arg(*l):
    return min(l)


def check_min(*l):
    lc = list(l)
    for i in lc:
        if i < 0 or i > 255:
            lc.remove(i)
    return min(lc)


def param(*tup, **dict):
    print(list(tup))
    print(dict)
    pass


param(7, 1, 5, 8, q=3, c=2)
print(fibonachi(8))

l = [10, 1, 5, -28, 1, 20]
print(min_arg(l[0], l[1], l[4], l[5]))
print(check_min(l[0], l[1], l[3], l[5]))
